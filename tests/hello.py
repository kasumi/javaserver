import socket

import Client_pb2
import Server_pb2

from google.protobuf.any_pb2 import Any

clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientsocket.settimeout(10)

clientsocket.connect(('localhost', 55551))

base_hello = Client_pb2.Base()
base_hello.data.Pack(Client_pb2.Hello())

clientsocket.send(base_hello.SerializeToString())

reply = clientsocket.recv(256)

if not reply:
    print "no answer"
    exit()

answer_base = Server_pb2.Base()
answer_base.ParseFromString(reply)

if answer_base.data.Is(Server_pb2.Hello().DESCRIPTOR):
    answer_hello = Server_pb2.Hello()
    answer_base.data.Unpack(answer_hello)

    print "Answer: ", answer_hello.message

