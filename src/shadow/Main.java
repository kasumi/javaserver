package shadow;

import shadow.core.Log;
import shadow.server.Server;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Server server = null;

        try {
            server = new Server(55551);
            server.start();
            Log.info("Start server... ");
        } catch (IOException e) {
            Log.fatal("Can't start server =(", e);
        }

        Server finalServer = server;
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Stop server... ");
                try {
                    finalServer.finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.info("Gracefull exit!");
            }
        });
    }
}
