package shadow.server;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import shadow.core.Log;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import shadow.server.ServerProtos;
import shadow.server.ClientProtos;

public class Worker extends Thread {
    private ByteBuffer buffer = ByteBuffer.allocate(256);
    private AtomicBoolean isFinish = new AtomicBoolean(false);
    private Selector selector = Selector.open();
    private ConcurrentLinkedQueue<SocketChannel> newChannels = new ConcurrentLinkedQueue<>();

    Worker(int id) throws Exception {
        this.setName("Worker " + id);
        Log.debug("[" + this.getName() + "] wait for connection...");
    }

    public void finish() throws Exception {
        this.isFinish.set(true);
        this.selector.wakeup();

        this.join();
    }

    public void run() {
        while (!this.isFinish.get()) {
            try {
                SocketChannel newChannel;
                while ((newChannel = this.newChannels.poll()) != null) {
                    newChannel.configureBlocking(false);
                    newChannel.register(this.selector, SelectionKey.OP_READ);
                }

                if (this.selector.select() == 0) {
                    continue;
                }

                Iterator it = this.selector.selectedKeys().iterator();
                while (it.hasNext()) {
                    SelectionKey key = (SelectionKey) it.next();
                    it.remove();

                    if (key.isReadable()) this.readHandler(key);
                }
            } catch (InvalidProtocolBufferException e) {
                Log.warn("[" + this.getName() + "] can't parse package");
            } catch (IOException e) {
                Log.warn("[" + this.getName() + "] some problems in the main loop...", e);
            }
        }
    }

    public void addChannel(SocketChannel socketChannel) throws IOException {
        this.newChannels.add(socketChannel);
        this.selector.wakeup();
    }

    private void readHandler(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        this.buffer.clear();

        while (socketChannel.read(this.buffer) > 0) {
            this.buffer.flip();

            ClientProtos.Base base_package = ClientProtos.Base.parseFrom(this.buffer);

            Any data = base_package.getData();
            if (data.is(ClientProtos.Hello.class)) {
                Log.debug("[" + this.getName() + "] Hello received");

                ServerProtos.HelloServer answer = ServerProtos.HelloServer.newBuilder()
                        .setMessage("Hello!").build();

                ServerProtos.BaseServer base_answer = ServerProtos.BaseServer.newBuilder()
                        .setData(Any.pack(answer)).build();

                ByteBuffer buffer = ByteBuffer.allocate(256);;
                buffer.put(base_answer.toByteArray());

                buffer.flip();
                socketChannel.write(buffer);
            }
            else {
                Log.warn("Receive unknown package");
            }
        }
    }
}
