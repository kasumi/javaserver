package shadow.server;

import shadow.core.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public class Server extends Thread {
    private ServerSocketChannel serverChannel;
    private AtomicBoolean isFinish = new AtomicBoolean(false);

    private Worker workers[] = new Worker[4];

    public Server(int port) throws IOException {
        this.setName("Server");

        this.serverChannel = ServerSocketChannel.open();
        this.serverChannel.socket().bind(new InetSocketAddress(port));
    }

    public void finish() throws Exception {
        for (Worker worker : this.workers)
            worker.finish();

        this.isFinish.set(true);
        if (this.serverChannel.isOpen())
            this.serverChannel.close();

        this.join();
    }

    public void run() {
        for (int i = 0, lim = this.workers.length; i < lim; ++i) {
            try {
                this.workers[i] = new Worker(i);
            } catch (Exception e) {
                Log.error("[" + this.getName() + "] Can't create worker...", e);
                return;
            }
        }

        for (Worker worker : this.workers)
            worker.start();

        while (!this.isFinish.get()) {
            try {
                this.acceptHandler();
            } catch (IOException e) {
                Log.warn("[" + this.getName() + "] Socket was closed...");
            }
        }
    }

    private void acceptHandler() throws IOException {
        final SocketChannel socketChannel = this.serverChannel.accept();
        if (socketChannel == null)
            return;

        Log.info("[" + this.getName() + "] Connected!");

        int id = new Random().nextInt(this.workers.length);
        this.workers[id].addChannel(socketChannel);
    }
}
