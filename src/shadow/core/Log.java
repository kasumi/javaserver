package shadow.core;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
    public static void debug(String message) {
        print(" debug: ", ANSI_GREEN, message, System.out);
    }

    public static void whisper(String message) {
        print(" debug: ", ANSI_GRAY, message, System.out);
    }

    public static void info(String message) {
        print("  info: ", ANSI_BLACK, message, System.out);
    }

    public static void note(String message) {
        print("  note: ", ANSI_BLUE, message, System.out);
    }

    public static void warn(Exception exception) {
        warn(getStackTrace(exception));
    }

    public static void warn(String message, Exception exception) {
        warn(getStackTrace(exception) + "\n" + message);
    }

    public static void warn(String message) {
        print("  warn: ", ANSI_YELLOW, message, System.out);
    }

    public static void error(Exception exception) {
        error(getStackTrace(exception));
    }

    public static void error(String message, Exception exception) {
        error(message + "\n" + getStackTrace(exception));
    }

    @SuppressWarnings("WeakerAccess")
    public static void error(String message) {
        print(" error: ", ANSI_RED, message, System.err);
    }

    public static void fatal(String message) {
        print(" fatal: ", ANSI_RED, message, System.err);
        System.exit(1);
    }

    public static void fatal(Exception exception) {
        fatal(getStackTrace(exception));
    }

    public static void fatal(String message, Exception exception) {
        fatal(message + "\n" + getStackTrace(exception));
    }

    private static String getStackTrace(final Throwable throwable) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        throwable.printStackTrace(pw);
        return sw.getBuffer().toString();
    }

    private static void print(String prefix, String color, String message, PrintStream stream) {
        String stamp = new SimpleDateFormat("[yyyy-MM-dd HH:mm:ss]").format(new Date());

        if (message.indexOf('\n') == -1) {
            stream.println(color + stamp + prefix + message + ANSI_RESET);
            return;
        }

        String[] lines = message.split("\n");
        int prefixLength = stamp.length() + prefix.length();
        String pad = String.format("%1$" + prefixLength + "s", " ");

        stream.print(color);
        stream.println(stamp + prefix + lines[0]);

        for (int i = 1, length = lines.length; i < length; ++i) {
            stream.print(pad);
            stream.print(lines[i]);
            stream.print('\n');
        }

        stream.print(ANSI_RESET);
    }

    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_BLACK = "\u001B[30m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_GRAY= "\u001B[37m";
}
