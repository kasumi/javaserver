# Java Server (very long project) #

Writing for fun and new knowledges.

### Build and run steps ###

* ./gradlew build
* ./gradlew run

### Using ###

* java 8
* protobuf 3.3.0 [protobuf](https://github.com/google/protobuf)
* gradle 4.1 [gradle](https://gradle.org/)
